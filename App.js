import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import { useState, useEfect } from "react" 
import Layout from "./layoutDeTelaEstrutura";
import Layouthorizontal from "./layouthorizontal"
import LayoutGrade from "./layoutGrade";
import ExpoStatusBar from "expo-status-bar/build/ExpoStatusBar";
import Components from "./components";

  

export default function App() {
  return (
    <View style={styles.container}>
      <Components></Components>
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
  },
});
